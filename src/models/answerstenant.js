const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * CustomAnswer: This model works as a helper for making dinamyc conversation's answers
 */
const answerstenant = new Schema({

    nodeName: {
        type: 'String',
        required: true
    },
    customID: {
        type: 'String',
        required: true
    },
    actionCustomDisplay:
    {
        type: 'String',
        required: false
    },
    registeredBy:
    {
        type: 'String',
        required: true
    },
    __v:
    {
        type: Number,
        required: true
    },
    tenant:
    {
        type: Schema.Types.ObjectId, ref: 'tenants', required: true
    },
    isDefault:
    {
        type: Boolean,
        required: false
    },
    defaultAnswer:
    {
        type: Schema.Types.ObjectId, ref: 'customAnswer', required: false
    }

}, { strict: false });


module.exports = mongoose.model('answerstenant', answerstenant)