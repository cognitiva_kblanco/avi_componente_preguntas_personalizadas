const mongoose = require('mongoose')
const Schema = mongoose.Schema;


const MenuCustomAnswers = new Schema(
    {
        cuid: {
            type: 'String',
            required: true
        },
        cuidParent: {
            type: 'String',
            required: false
        },
        nodeName: {
            type: 'String',
            required: true
        },
        menuName: {
            type: 'String',
            required: true
        },
        label: {
            type: 'String',
            required: true
        },
        order: {
            type: 'Number',
            required: true
        },
        createdBy: {
            type: 'String',
            required: true
        },
        dateAdded: {
            type: 'Date',
            default: Date.now,
            required: true
        },
    });

module.exports = mongoose.model('MenuCustomAnswers', MenuCustomAnswers)
