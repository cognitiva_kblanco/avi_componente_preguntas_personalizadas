const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const tenant = new Schema(
    {
        cuid: {
            type: 'String',
            required: true
        },
        fullName: {
            type: 'String',
            required: true
        },
        email: {
            type: 'String',
            required: true
        },
        organization: {
            type: 'String',
            required: true
        },
        phone: {
            type: 'String',
            required: false
        },
        country: {
            type: Schema.Types.ObjectId, ref: 'country',
            required: true
        },
        region: {
            type: Schema.Types.ObjectId, ref: 'region'
        },
        createdBy: {
            type: 'String',
            required: false
        },
        createdAt: {
            type: 'Date',
            default: Date.now,
            required: true
        },
        updatedBy: {
            type: 'String',
            required: false
        },
        updatedAt: {
            type: 'Date',
            required: false
        },
        activatedAt: {
            type: 'Date',
            required: false
        },
        expiredAt: {
            type: 'Date',
            required: false
        },
        status: {
            type: String,
            enum: ['PENDIENTE', 'ACTIVO', 'EXPIRADO', 'CANCELADO', 'ELIMINADO']
        },
        comment: {
            type: String,
            maxlength: 150,
            required: false
        }
    });

tenant.path('comment').validate(function (v) {
    return v.length < 150;
});

tenant.statics.getAllIds = function (callback, estado) {
    let tenants = []
    this.find({ status: estado }).exec((err, results) => {
        let newArr = results.map(function (element) {
            var elemento = element.toObject();
            var idObjeto = mongoose.Types.ObjectId(elemento._id);
            return idObjeto
        });
        callback(err, newArr);
    });
}

module.exports = mongoose.model('tenant', tenant)
