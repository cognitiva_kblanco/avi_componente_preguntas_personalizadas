const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * CustomAnswer: This model works as a helper for making dinamyc conversation's answers
 */
const customAnswerSchema = new Schema({
    tenant:
    {
        type: Schema.Types.ObjectId, ref: 'tenant'
    },
    nodeName: {
        type: 'String',
        required: true
    },
    customID: {
        type: 'String',
        required: true
    },
    actionCustomDisplay:
    {
        type: 'String',
        required: false
    },
    registeredBy:
    {
        type: 'String',
        required: true
    },
    __v:
    {
        type: Number
    },
    defaultAnswer:
    {
        type: Boolean,
        required: false
    }

}, { strict: false });



module.exports = mongoose.model('customAnswer', customAnswerSchema)