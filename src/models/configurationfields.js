const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const configField = new Schema({
    user: {
        type: Schema.Types.ObjectId, ref: 'User',
        required: false
    },
    label: {
        type: 'String',
        required: true
    },
    value: {
        type: 'String',
        required: true
    },
    createdBy: {
        type: 'String',
        required: true
    },
    dateAdded: {
        type: 'Date',
        required: true,
        default: Date.now
    },
    updatedBy: {
        type: 'String',
        required: true
    },
    updatedAt: {
        type: 'Date',
        required: true
    }
});
module.exports = mongoose.model('configurationfields', configField)