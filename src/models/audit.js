const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const Audit = new Schema({
    timestamp: {
        type: 'Date',
        default: Date.now,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId, ref: 'User',
        required: false
    },
    tenant: {
        type: Schema.Types.ObjectId, ref: 'tenant',
        required: false
    },
    action: {
        type: 'String',
        required: true
    },
    component: {
        type: 'String',
        required: true
    },
    responseCode: {
        type: Number,
        required: true
    },
    response: {
        type: 'String',
        required: false
    },
    event: {
        type: 'String',
        required: true
    },
    error: {
        type: 'String',
        required: false
    }
}, { strict: false });
module.exports = mongoose.model('Audit', Audit)