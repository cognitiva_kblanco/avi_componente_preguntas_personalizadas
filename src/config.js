const path = require('path');
const config = {
    assets: path.join(__dirname, 'assets/'),
    feedMenuElements: false,
    language: 'es'
};

module.exports = config;
