const MenuCustomAnswers = require('../controllers/menuCustomAnswers');
const express = require('express')
const router = express.Router()
const serverConfig = require('../config')
/*
* @author: ASANCHEZ 2017
*/
const controller = require('../controllers/main');

router.get('/get', MenuCustomAnswers.getListByParentId);
router.get('/menus/:id', MenuCustomAnswers.getAnswersTenantMenu);

module.exports = router;
