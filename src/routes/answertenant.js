const answertenant = require('../controllers/answertenant');
const express = require('express')
const router = express.Router()



router.post('/initialize/:id', answertenant.initializeAnswersTenant);
router.get('/answers', answertenant.getAnswers);
router.get('/byNodeName', answertenant.getCustomAnswerByNodeName);
router.put('/:id', answertenant.updateAnswer);

module.exports = router;
