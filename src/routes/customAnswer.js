const CustomAnswers = require('../controllers/customAnswers');
const express = require('express')
const router = express.Router()
/*
* @author: ASANCHEZ 2017
*/
const controller = require('../controllers/main');

router.post('/add', CustomAnswers.addCustomAnswers);
router.get('/all', CustomAnswers.getCustomAnswers);
router.get('/get', CustomAnswers.getCustomAnswer);
router.delete('/delete', CustomAnswers.deleteCustomAnswer);
router.put('/update', CustomAnswers.updateCustomAnswer);

module.exports = router;
