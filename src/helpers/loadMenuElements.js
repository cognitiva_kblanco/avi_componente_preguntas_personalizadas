const serverConfig = require('../config');
const MenuCustomAnswers = require('../models/menuCustomAnswers');


/**
 * Este modulo carga los elementos del menu para la carga de respuestas custom
 */
const feedMenuElements = async (req, res) => {
    var _ = require("underscore");
    var fs = require("fs");
    var path = serverConfig.assets + 'menuFields.json';
    var jsonObject = JSON.parse(fs.readFileSync(path, 'utf8'));
    try {
        deletedElements = await deleteElementsInCollection();// Deleting all elements in collection
        _.map(jsonObject, function (content) {
            _.map(content, function (data) {
                var objectCustom = new MenuCustomAnswers(data);
                console.log('Inserting into mongodb - menu label: ' + data.nodeName);
                MenuCustomAnswers.create([objectCustom], (error) => {
                    if (error) {
                        console.log(error);
                    }
                });
            });
        });
    } catch (err) {
        return res.status(500).send(err)
    }
}
/**
 * Delete all elementes in CustomAnswer collection
 * @param {*} callBack 
 */
function deleteElementsInCollection() {
    try {
        MenuCustomAnswers.collection.remove();
        return true;
    } catch (error) {
        console.log('Error while trying to delete');
        return false
    }

}

module.exports = {
    feedMenuElements
}
