const CustomAnswer = require('../models/customAnswer');
const cuid = require('cuid');
const sanitizeHtml = require('sanitize-html');
const Audit = require('../models/audit');
const AuditController = require('../controllers/audit');
const tenant = require('../models/tenant');
const answerTenants = require('../controllers/answertenant');


/**
 * Get all CustomAnswers
 * @param req
 * @param res
 * @returns void
 */
function getCustomAnswers(req, res) {


    CustomAnswer.find().sort('-dateAdded').exec((err, CustomAnswers) => {
        if (err) {
            AuditController.save(req, res, null, 'customAnswers', 'getCustomAnswers', null, 500, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
            res.status(500).send(err);
        }
        res.json({ CustomAnswers });
    });
}



/**
 * Get a single CustomAnswer
 * @param {*} nodeName 
 */
function getCustomAnswer(req, res) {
    CustomAnswer.findOne({ nodeName: req.query.nodeName, tenant: req.query.tenant }).sort('-dateAdded').exec((err, CustomAnswer) => {
        if (err) {
            AuditController.save(req, res, null, 'customAnswers', 'getCustomAnswer', null, 500, req.query.tenant, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
            res.status(500).send(err);
        }
        res.json({ CustomAnswer });
    });
}



/**
 * Update CustomAnswer's state to disable
 * @param req
 * @param res
 * @returns void
 */
function deleteCustomAnswer(req, res) {
    try {
        CustomAnswer.update({ nodeName: req.query.nodeName, tenant: req.query.tenant },
            { $set: { state: false } },
            { upsert: true, minimize: false }).exec((err, Dialog) => {
                if (err) {
                    AuditController.save(req, res, null, 'cusomAnswers', 'deleteCustomAnswer', null, res.statusCode, req.query.tenant, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
                    return res.json({ result: -1 });
                }
                return res.json({ result: 0 });
            });
    } catch (err) {
        return res.json({ result: -1 });
    }
}



/**
 * Update a CustomAnswer
 * @param {*} req 
 * @param {*} res 
 * @param {*} req.body.nodeName
 * @param {*} req.body.tenant
 * @param {*} req.body.updatedBy
 */
const updateCustomAnswer = async (req, res) => {
    try {
        let now = new Date();
        CustomAnswer.update({ nodeName: req.body.nodeName, tenant: req.body.tenant },
            { $set: { state: true, value: req.body.Answersarray, updatedBy: req.body.updatedBy, updatedAt: now } },
            { upsert: true, minimize: false }).exec((err, Dialog) => {
                if (err) {
                    AuditController.save(req, res, null, 'cusomAnswers', 'updateCustomAnswer', null, res.statusCode, req.query.tenant, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
                    res.json({ result: -1 });
                }
                res.json({ result: 0 });
            });
    } catch (err) {
        return res.status(500).send(err)
    }
}


/**
 * add CustomAnswer into database
 * @param {*} req 
 * @param {*} res 
 */
function addCustomAnswers(req, res) {

    if (req.body.nodeName == null || req.body.customID == null || req.body.Answersarray == null || req.body.registeredBy == null) {
        AuditController.save(req, res, null, 'customAnswers', 'addCustomAnswers', null, 422, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', 'No están los datos completos para realizar la petición');
        res.json({ result: -1 });
    }
    else {
        const newCustomAnswer = new CustomAnswer({
            nodeName: req.body.nodeName,
            customID: req.body.customID,
            defaultNotFound: req.body.defaultNotFound,
            value: req.body.Answersarray,
            actionCustomDisplay: req.body.actionCustomDisplay,
            registeredBy: req.body.registeredBy,
        });

        // Let's sanitize inputs
        newCustomAnswer.nodeName = sanitizeHtml(newCustomAnswer.nodeName);
        newCustomAnswer.customID = sanitizeHtml(newCustomAnswer.customID);
        newCustomAnswer.defaultNotFound = sanitizeHtml(newCustomAnswer.defaultNotFound);
        newCustomAnswer.actionCustomDisplay = sanitizeHtml(newCustomAnswer.actionCustomDisplay);
        newCustomAnswer.registeredBy = sanitizeHtml(newCustomAnswer.registeredBy);




        newCustomAnswer.save((err, saved) => {
            if (err) {
                AuditController.save(req, res, null, 'customAnswers', 'addCustomAnswers', null, 500, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
                res.json({ result: -1 });
            }
            else {
                tenant.getAllIds(async function (err, ids) {
                    if (err) {
                        AuditController.save(req, res, null, 'customAnswers', 'addCustomAnswers', null, 500, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
                        res.json({ result: -1 });
                    }
                    else {
                        let objetoAnswer = newCustomAnswer.toObject();
                        let idCustomAnswer = objetoAnswer._id;
                        objetoAnswer.defaultAnswer = idCustomAnswer;
                        delete objetoAnswer._id;
                        let respuesta = await answerTenants.addMany(ids, objetoAnswer);
                    }
                }, 'ACTIVO');

                console.log(saved)
                res.json({ result: 0 });
            }
        });
    }
}



module.exports = {
    getCustomAnswers,
    getCustomAnswer,
    deleteCustomAnswer,
    updateCustomAnswer,
    addCustomAnswers
}