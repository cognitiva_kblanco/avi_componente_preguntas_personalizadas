const tenant = require('../models/tenant');
const CustomAnswer = require('../models/customAnswer');
const answerstenant = require('../models/answerstenant');
const mongoose = require('mongoose');
const Audit = require('../models/audit');
const configfields = require('../models/configurationfields');
const AuditController = require('../controllers/audit');


async function initializeAnswersTenant(req, res) {

    let idTenant = req.params.id;
    let query = CustomAnswer.find({ customID: '&CA_1' });

    let answers = await query.exec();
    let newArr = answers.map(function (element) {

        let idObjeto = mongoose.Types.ObjectId(idTenant);
        let elemento = element.toObject();
        elemento.tenant = idObjeto;
        elemento.isDefault = true;
        elemento.defaultAnswer = elemento._id;
        delete elemento._id;

        return elemento;
    });

    answerstenant.collection.insertMany(newArr, function (err, docs) {
        if (err) {
            AuditController.save(req, res, null, 'answertenant', 'initializeAnswersTenant', null, 500, idTenant, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
            res.status(500).send(err);
        }
    });

    res.json({ success: 1 });

}



/**
 * Get a single CustomAnswer
 * @param {*} nodeName 
 */
function getCustomAnswerByNodeName(req, res) {

    let nodeName = req.query.nodeName;
    let tenant = req.query.tenant;

    if (!nodeName || !tenant) {

        AuditController.save(req, res, null, 'answertenant', 'getCustomAnswer', null, 422, idTenant, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', "Se deben enviar los campos necesarios para hacer este request");
        res.status(422).send({ error: "Se deben enviar los campos de NodeName y Tenant necesarios para hacer este request" });
    }

    answerstenant.findOne({ nodeName: nodeName, tenant: tenant }).sort('-dateAdded').exec((err, CustomAnswer) => {
        if (err) {
            AuditController.save(req, res, null, 'answertenant', 'getCustomAnswer', null, 500, req.query.tenant, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
            res.status(500).send(err);
        }
        res.json({ CustomAnswer });
    });
}




/**
 * Gets all CustomAnswer
 * @param {*} nodeName 
 */
async function getAnswers(req, res) {

    let idTenant = req.query.idTenant;
    if (!idTenant) {

        AuditController.save(req, res, null, 'answertenant', 'getAnswers', null, 422, idTenant, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', "Se deben enviar los campos necesarios para hacer este request");
        res.status(422).send({ error: "Se deben enviar los campos necesarios para hacer este request" });
    }
    else {

        try {
            let query = {};
            query.tenant = new mongoose.Types.ObjectId(req.query.idTenant);
            if (req.query.isDefault != null) {
                query.isDefault = req.query.isDefault
            }
            if (typeof req.query.nodeName !== 'undefined' && req.query.nodeName) {
                query.nodeName = req.query.nodeName
            }
            let answers = await answerstenant.find(query).populate('defaultAnswer').exec();

            res.json(answers);
        }
        catch (err) {
            AuditController.save(req, res, null, 'answertenant', 'getAnswers', null, 500, idTenant, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
            res.status(500).send({ error: { err } });
        }
    }

}




async function updateAnswer(req, res) {


    if (typeof req.body.isDefault === 'undefined') {
        AuditController.save(req, res, null, 'answertenant', 'updateAnswer', null, 422, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', "Se deben enviar los campos necesarios para hacer este request");
        res.status(422).send({ error: "Se deben enviar los campos necesarios para hacer este request" });
    }
    else if (typeof req.body.isDefault !== 'undefined' && typeof req.body.isDefault !== 'boolean') {
        AuditController.save(req, res, null, 'answertenant', 'updateAnswer', null, 422, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', "El tipo de dato no es el correcto");
        res.status(422).send({ error: "El tipo de dato no es el correcto" });
    }
    else {

        try {

            let answer_id = req.params.id;
            let newId = new mongoose.mongo.ObjectId(answer_id);

            answerstenant.findByIdAndUpdate(newId,
                { $set: { value: req.body.Answersarray, updatedAt: new Date(), isDefault: false } },
                { upsert: true, minimize: false }).exec((err, answer) => {
                    if (err) {
                        AuditController.save(req, res, null, 'answertenant', 'updateAnswer', null, 500, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
                        return res.status(500).send(respuesta);
                    }
                    res.status(200).send({ data: answer })

                });


        }

        catch (err) {

            var respuesta = {}
            respuesta.error = err;
            AuditController.save(req, res, null, 'answertenant', 'updateAnswer', null, 500, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
            return res.status(500).send(respuesta);
        }
    }

}




async function addMany(idsTenants, newCustomAnswer) {
    return new Promise(resolve => {
        let respuestas = [];
        console.log(idsTenants);
        for (let i = 0; i < idsTenants.length; i++) {
            let inicial = Object.assign({}, newCustomAnswer);;
            let objetoInsertar = inicial;
            let elemento = idsTenants[i]
            objetoInsertar.tenant = elemento;
            respuestas.push(objetoInsertar);
        }
        answerstenant.collection.insertMany(respuestas, function (err, docs) {
            if (err) {
                AuditController.save(req, res, null, 'answertenant', 'addMany', null, 500, idTenant, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
            }
        });
    });
}

module.exports = {
    initializeAnswersTenant,
    getAnswers,
    updateAnswer,
    addMany,
    getCustomAnswerByNodeName
}

