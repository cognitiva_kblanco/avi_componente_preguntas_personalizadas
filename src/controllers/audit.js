const Audit = require('../models/audit');
const sanitizeHtml = require('sanitize-html');

/**
 * Get all audits/logs
 * @param {*} req
 * @param {*} res
 */
const all = (req, res) => {
    Audit.find().populate('user', 'name').exec((err, audits) => {
        if (err) {
            res.status(500).send(err);
        }
        res.json({ result: audits });
    });
}

const save = async (req, res, user, modulo, action, response, responseCode, tenant, component, event, error) => {


    if (component == null || action == null || responseCode == null || event == null || modulo == null) {
        return res.status(422).send({ error: 'Error almacenando datos de auditoría.' });
    }
    const audit = new Audit({
        user: user,
        action: action,
        response: response,
        responseCode: responseCode,
        tenant: tenant,
        component: component,
        event: event,
        error: error,
        modulo: modulo
    });

    // Let's sanitize inputs
    if (user != null) {
        audit.user = sanitizeHtml(audit.user);
    }
    audit.action = sanitizeHtml(audit.action);
    audit.response = sanitizeHtml(audit.response);
    if (tenant != null) {
        audit.tenant = sanitizeHtml(audit.tenant);
    }
    if (response != null) {
        audit.response = sanitizeHtml(audit.response);
    }
    audit.component = sanitizeHtml(audit.component);
    audit.modulo = sanitizeHtml(audit.modulo);
    audit.event = sanitizeHtml(audit.event);
    if (error != null) {
        audit.error = sanitizeHtml(error);
    }
    audit.save((err) => {
        if (err) {
            return false;
        }
        return true
    });
}
module.exports = {
    all,
    save
}