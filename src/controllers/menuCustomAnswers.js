const MenuCustomAnswer = require('../models/menuCustomAnswers');
const cuid = require('cuid');
const sanitizeHtml = require('sanitize-html');
const answerstenant = require('../models/answerstenant');
const AuditController = require('../controllers/audit');

/**
 * Obtiene todas los elementos almacenados en la coleccion 
 * @param req
 * @param res
 * @returns void
 */
var result;
function getMenuCustomAnswers(callBack) {
    var element;
    MenuCustomAnswers.find().cursor()
        .on('data', function (doc) {
            element = doc.toObject();

            // handle doc
        })
        .on('error', function (err) {
            // handle error
        })
        .on('end', function () {
            // final callback
            callBack(null, element);
        });
}

/**
 * guarda un MenuCustomAnswers
 * @param req
 * @param res
 * @returns void
 */
function addMenuCustomAnswer(req, res) {

    const newMenuCustomAnswers = new MenuCustomAnswer({
        cuidParent: req.query.cuidParent,
        menuName: req.query.menuName,
        label: req.query.label,
        nodeName: req.query.nodeName,
        order: req.query.order,
        createdBy: req.query.createdBy,

    });

    // Let's sanitize inputs
    newMenuCustomAnswers.cuid = cuid();
    newMenuCustomAnswers.cuidParent = sanitizeHtml(newMenuCustomAnswers.cuidParent);
    newMenuCustomAnswers.menuName = sanitizeHtml(newMenuCustomAnswers.menuName);
    newMenuCustomAnswers.order = sanitizeHtml(newMenuCustomAnswers.order);
    newMenuCustomAnswers.label = sanitizeHtml(newMenuCustomAnswers.label);
    newMenuCustomAnswers.createdBy = sanitizeHtml(newMenuCustomAnswers.createdBy);
    newMenuCustomAnswers.nodeName = sanitizeHtml(newMenuCustomAnswers.nodeName);

    newMenuCustomAnswers.save((err, saved) => {
        if (err) {
            res.status(500).send(err);
        }
        res.json({ MenuCustomAnswers: saved });
    });
}

/**
 * Obtiene un documento del schema buscando por cuid
 * @param req
 * @param res
 * @returns void
 */
function getMenuCustomAnswer(req, res) {
    MenuCustomAnswers.findOne({ cuid: req.query.id }).exec((err, MenuCustomAnswers) => {
        if (err) {
            AuditController.save(req, res, null, 'menuCustomAnswers', 'getMenuCustomAnswer', null, 500, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
            res.status(500).send(err);
        }
        res.json({ MenuCustomAnswers });
    });
}

/**
 * Delete a MenuCustomAnswers
 * @param req
 * @param res
 * @returns void
 */

function deleteMenuCustomAnswer(req, res) {
    MenuCustomAnswers.findOne({ cuid: req.query.cuid }).exec((err, MenuCustomAnswers) => {
        if (err) {
            AuditController.save(req, res, null, 'menuCustomAnswers', 'deleteMenuCustomAnswer', null, 500, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
            res.status(500).send(err);
        }

        MenuCustomAnswers.remove(() => {
            res.status(200).end();
        });
    });
}

/**
 * Obtiene los elementos relacionados a un parent id 
 * @param req (parentId,menu)
 * @param res
 * @returns void
 */
function getListByParentId(req, res) {
    let parentId = req.query.parentId || null;
    let menuName = req.query.menu
    MenuCustomAnswer.find({ cuidParent: parentId, menuName: menuName.toUpperCase() }).sort({ order: 1 }).exec((err, MenuCustomAnswer) => {
        if (err) {
            AuditController.save(req, res, null, 'menuCustomAnswers', 'getListByParentId', null, 500, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
            res.status(500).send(err);
        }
        res.json({ result: MenuCustomAnswer });
    });
}


async function getAnswersTenantMenu(req, res) {

    const mongoose = require('mongoose');

    let tenant = new mongoose.Types.ObjectId(req.params.id);
    let isDefault = req.query.isDefault;
    let menuName = req.query.menu;

    if (typeof isDefault === 'undefined') {
        AuditController.save(req, res, null, 'menuCustomAnswer', 'getAnswersTenantMenu', null, 422, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', "Se deben enviar los campos necesarios para hacer este request");
        res.status(422).send({ error: "Se deben enviar los campos necesarios para hacer este request" });
    }
    else {
        let boolDefault = (isDefault == 'true');
        try {
            let answerTenants = await answerstenant.find({ isDefault: boolDefault, tenant: tenant }).select('nodeName').exec();
            let newArr = answerTenants.map(function (element) {
                let elemento = element.toObject();
                return elemento.nodeName;
            });
            let result = await MenuCustomAnswer.find({ menuName: menuName.toUpperCase(), nodeName: { $in: newArr } }).exec();
            res.json(result);
        }
        catch (err) {
            AuditController.save(req, res, null, 'menuCustomAnswer', 'getAnswersTenantMenu', null, 500, null, 'COMPONENTE_PREGUNTAS_PERSONALIZADAS', 'ERROR', err);
            res.status(500).send({ error: err });
        }
    }
}

module.exports = {
    getMenuCustomAnswers,
    addMenuCustomAnswer,
    getMenuCustomAnswer,
    deleteMenuCustomAnswer,
    getListByParentId,
    getAnswersTenantMenu
}